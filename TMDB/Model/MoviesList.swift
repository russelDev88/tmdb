//
//  MoviesList.swift
//  TMDB
//
//  Created by Russel Abdul Gafoor on 17/12/19.
//  Copyright © 2019 Russel Abdul Gafoor. All rights reserved.
//

import Foundation
import Alamofire

/* API Response to item for http https://api.themoviedb.org/4/list/2?api_key=75e9572629ee6934be7ea50d6540c5ee looks like:
 {
 
 "poster_path": "/i0mpvMIIjyubXsVKug9vX0lYpnd.jpg",
 "popularity": 12.412,
 "vote_count": 1935,
 "video": false,
 "media_type": "movie",
 "id": 74643,
 "adult": false,
 "backdrop_path": "/tr6OExnBfjLADi9OyZoW308mPAp.jpg",
 "original_language": "en",
 "original_title": "The Artist",
 "genre_ids": [
 35,
 18,
 10749
 ],
 "title": "The Artist",
 "vote_average": 7.4,
 "overview": "Hollywood, 1927: As silent movie star George Valentin wonders if the arrival of talking pictures will cause him to fade into oblivion, he sparks with Peppy Miller, a young dancer set for a big break.",
 "release_date": "2011-06-26"
 */



enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
}

protocol MoviesViewModelDelegate: class {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
    func onFetchFailed(with reason: String)
}

enum MovieListFields: String {
    case PosterPath = "poster_path"
    case Popularity = "popularity"
    case VoteCount = "vote_count"
    case Video = "video"
    case MediaType = "media_type"
    case ID = "id"
    case Adult = "adult"
    case BackdropPath = "backdrop_path"
    case OriginalLanguage = "original_language"
    case OriginalTitle = "original_title"
    case GenreIds = "genre_ids"
    case Title = "title"
    case VoteAverage = "vote_average"
    case Overview = "overview"
    case ReleaseDate = "release_date"
}

class MoviesListWrapper
{
    
    var movies: [MoviesList]?
    var categoryID: Int?
    var page: Int?
    var totalPages: Int?
    var totalResults: Int?
    var createdBy: String?
    var description: String?
    var name: String?
    var posterPath: String?
    var sortBy : String?
    
    
    
}

class MoviesList {
    
    private weak var delegate: MoviesViewModelDelegate?
    
    
    
    var idNumber: Int?
    var originalTitle: String?
    var title: String?
    var voteCount: Int?
    var popularity: String?
    var posterPath: String? // TODO: parse into array
    var video: Bool? // TODO: array
    var adult: Bool?
    var mediaType: String?
    var backdropPath: String?
    var originalLanguage: String?
    var genreIds: [String]?
    var VoteAverage: String?
    var Overview: String?
    var ReleaseDate:Date?
    var ReleaseYear: Int!
    
    
    required init(json: [String: Any]) {
        self.title = json[MovieListFields.Title.rawValue] as? String
        self.originalTitle = json[MovieListFields.OriginalTitle.rawValue] as? String
        self.posterPath = json[MovieListFields.PosterPath.rawValue] as? String
        self.originalLanguage = json[MovieListFields.OriginalLanguage.rawValue] as? String
        self.idNumber = json[MovieListFields.ID.rawValue] as? Int
        
        let releaseDate = json[MovieListFields.ReleaseDate.rawValue] as? String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.ReleaseDate = dateFormatter.date(from: releaseDate!)
        dateFormatter.dateFormat = "yyyy"
        self.ReleaseYear = Int(dateFormatter.string(from: self.ReleaseDate!))
        
        // TODO: more fields!
    }
    
    // MARK: Endpoints
    class func endpointForPage(_ pageNumber: Int) -> String {
        return "\(listingAPI)?page=\(pageNumber)&api_key=\(apiKey)"
    }
    
    
    // MARK: CRUD
    
    
    // GET / Read  movies
    fileprivate class func getMoviesAtPath(_ path: String, completionHandler: @escaping (Result<MoviesListWrapper>) -> Void) {
        // make sure it's HTTPS because sometimes the API gives us HTTP URLs
        guard var urlComponents = URLComponents(string: path) else {
            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
            completionHandler(.failure(error))
            return
        }
        urlComponents.scheme = "https"
        guard let url = try? urlComponents.asURL() else {
            let error = BackendError.urlError(reason: "Tried to load an invalid URL")
            completionHandler(.failure(error))
            return
        }
        let _ = Alamofire.request(url)
            .responseJSON { response in
                if let error = response.result.error {
                    completionHandler(.failure(error))
                    return
                }
                let moviesWrapperResult = MoviesList.moviesArrayFromResponse(response)
                completionHandler(moviesWrapperResult)
        }
    }
    
    class func getMovies(_ pageNumber: Int,_ completionHandler: @escaping (Result<MoviesListWrapper>) -> Void) {
        
        getMoviesAtPath((MoviesList.endpointForPage(pageNumber)), completionHandler: completionHandler)
    }
    
    
    private class func moviesArrayFromResponse(_ response: DataResponse<Any>) -> Result<MoviesListWrapper> {
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print(response.result.error!)
            return .failure(response.result.error!)
        }
        
        // make sure we got JSON and it's a dictionary
        guard let json = response.result.value as? [String: Any] else {
            print("didn't get movies object as JSON from API")
            return .failure(BackendError.objectSerialization(reason:
                "Did not get JSON dictionary in response"))
        }
        
        let wrapper:MoviesListWrapper = MoviesListWrapper()
        wrapper.createdBy = json["created_by"] as? String
        wrapper.description = json["description"] as? String
        wrapper.name = json["name"] as? String
        wrapper.posterPath = json["poster_path"] as? String
        wrapper.categoryID = json["id"] as? Int
        wrapper.page = json["page"] as? Int
        wrapper.totalPages = json["total_pages"] as? Int
        wrapper.totalResults = json["total_results"] as? Int
        wrapper.sortBy = json["sort_by"] as? String
        var allMovies: [MoviesList] = []
        if let results = json["results"] as? [[String: Any]] {
            for jsonMovies in results {
                let movies = MoviesList(json: jsonMovies)
                allMovies.append(movies)
            }
        }
        wrapper.movies = allMovies
        return .success(wrapper)
    }
}

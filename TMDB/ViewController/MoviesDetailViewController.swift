//
//  MoviesDetailViewController.swift
//  TMDB
//
//  Created by Russel Abdul Gafoor on 17/12/19.
//  Copyright © 2019 Russel Abdul Gafoor. All rights reserved.
//

import UIKit

class MoviesDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var moviesImageView: UIImageView!
    @IBOutlet weak var movieTitleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    
    
    
    
    var moviesDetail: MoviesList?
    var movieID: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.largeTitleDisplayMode = .never
        // Do any additional setup after loading the view.
        
        self.loadMovieDetials()
        
    }
    
    
    func loadMovieDetials() {
        self.activityIndicatorView .startAnimating()
        MoviesDetail.getMoviesDetail(moviesDetail) {  result in
            if let error = result.error {
                // TODO: improved error handling
                let alert = UIAlertController(title: "Error", message: "Could not load first movies :( \(error.localizedDescription)", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            let movieDetails = result.value
            self.loadMovieDetials(movieDetails)
           
        }
    }
    
    
    func loadMovieDetials(_ details: MoviesDetail?) {
        
        self.activityIndicatorView .stopAnimating()
        self.titleLabel.text = "\(String(describing: details!.originalTitle ?? "")) (\(String(describing: details!.releaseYear ?? 0)))"
        self.subTitleLabel.text = details?.tagLine
        self.overViewLabel.text = details?.overview
        var description = ""
        
        
        if (details!.runTime != nil)
        {
            let runTimeinSeconds = Double (details!.runTime!)*60
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.hour, .minute,]
            formatter.unitsStyle = .abbreviated
            let runtime =  formatter.string(from: runTimeinSeconds)
            
            
            description = description + "Duration : \(runtime!) \n"
            
        }
        
        
        for genreDictionary in details!.genres! {
            let content = genreDictionary["name"] as! String
            description = description + "\(content) | "
        }
        
        let formatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale 
        formatter.numberStyle = .currency
        
        if let formattedBudget = formatter.string(from: NSNumber(value: details!.budget!)) {
            description = description +  "\n Budget: \(formattedBudget)"
        }
        if let formattedBudget = formatter.string(from: NSNumber(value: details!.revenue!)) {
            description = description +  "\n Revenue: \(formattedBudget)"
        }
        if (details?.releaseDate != nil)
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let releaseDate = dateFormatter.string(from: details!.releaseDate!)
            if  details!.status == "Released"{
                description = description +  "\n Released Date: \(releaseDate)"}
            else
            {
                description = description +  "\n Releasing Date: \(releaseDate)"}
        }
        
        self.descriptionLabel.text = description
        
        //  let description =
        self.overViewLabel.text = details?.overview
        let  imageWidth = 100 * Int(round(self.moviesImageView.frame.size.width / 100.0))
        let  moviesImagePath = "https://image.tmdb.org/t/p/w\(imageWidth)" + details!.posterPath!
        
        if  let url =  URL(string:
            moviesImagePath)
        {
            
            
            if let image = try? UIImage(data: Data(contentsOf: url )) {
                self.moviesImageView?.image = image
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

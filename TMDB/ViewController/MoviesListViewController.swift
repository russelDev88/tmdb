//
//  MoviesListViewController.swift
//  TMDB
//
//  Created by Russel Abdul Gafoor on 17/12/19.
//  Copyright © 2019 Russel Abdul Gafoor. All rights reserved.
//

import UIKit

class MoviesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITableViewDataSourcePrefetching,UITextFieldDelegate{
    
    
    
    @IBOutlet var filterToolBar: UIToolbar!
    @IBOutlet weak var fromYearTextField: UITextField!
    @IBOutlet weak var toYearTextField: UITextField!
    var selectedTextField : UITextField?
    var movies: [MoviesList]?
    var filteredMovies:[ MoviesList]?
    var moviesWrapper: MoviesListWrapper? // holds the last wrapper that we've loaded
    var isLoadingMovies = false
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(MoviesListViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.listTableView.addSubview(self.refreshControl)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationItem.title = "TMDB"
        self.listTableView.contentInset = UIEdgeInsets.init(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0);
        self.listTableView.isHidden = true
        self.listTableView.dataSource = self
        self.listTableView.prefetchDataSource = self
        self.activityIndicatorView .startAnimating();
        self.loadMovies()
        self.filterToolBar.isHidden = true
        //       self.fromYearTextField.inputAccessoryView = self.filterToolBar
        //       self.toYearTextField.inputAccessoryView = self.filterToolBar
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.movies=nil
        self.moviesWrapper=nil
        self.loadMovies()
        refreshControl.endRefreshing()
    }
    // MARK: Loading Species from API
    func loadMovies() {
        isLoadingMovies = true
        var pageNumber : Int! = 0
        
        if (self.moviesWrapper != nil)
        {
            pageNumber = self.moviesWrapper!.page!
        }
        MoviesList.getMovies(pageNumber+1)  { result in
            self.activityIndicatorView .stopAnimating();
            if let error = result.error {
                // TODO: improved error handling
                self.isLoadingMovies = false
                let alert = UIAlertController(title: "Error", message: "Could not load first movies :( \(error.localizedDescription)", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            let moviesWrapper = result.value
            self.addMoviesFromWrapper(moviesWrapper)
            self.isLoadingMovies = false
            self.filteredMovies = self.movies
            self.listTableView?.reloadData()
        }
    }
    
    
    func addMoviesFromWrapper(_ wrapper: MoviesListWrapper?) {
        self.listTableView.isHidden = false
        self.moviesWrapper = wrapper
        if self.movies == nil {
            self.movies = self.moviesWrapper?.movies
        } else if self.moviesWrapper != nil && self.moviesWrapper!.movies != nil {
            self.movies = self.movies! + self.moviesWrapper!.movies!
        }
        
        let movieWithMaxAndMinYears = getMaximumAndMinimum()
        self.fromYearTextField.text = String(describing: movieWithMaxAndMinYears.oldMovieRelease.ReleaseYear ?? 0)
        self.toYearTextField.text = String(describing: movieWithMaxAndMinYears.latestMovieRelease.ReleaseYear ?? 0)
    }
    
    // MARK: TableViewDataSource
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.moviesWrapper != nil
        {
            return "\(String(describing: self.moviesWrapper?.name ?? "") ) \n \(String(describing: self.moviesWrapper?.description ?? "") )" 
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filteredMovies == nil {
            return 0
        }
        return self.filteredMovies!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListTableViewCell", for: indexPath) as? MovieListTableViewCell
        if self.filteredMovies != nil && self.filteredMovies!.count >= indexPath.row
        {
            let movies = self.filteredMovies![indexPath.row]
            cell?.movietitleLabel?.text = "\(String(describing: movies.originalTitle ?? "")) (\(String(describing: movies.ReleaseYear ?? 0)))"
            
            
            cell?.movieSubtitleLabel?.text = "Language : \(String(describing: movies.originalLanguage ?? ""))"
            
            let  moviesImagePath = "https://image.tmdb.org/t/p/w200" + movies.posterPath!
            
            URLSession.shared.dataTask( with: NSURL(string:moviesImagePath)! as URL, completionHandler: {
                (data, response, error) -> Void in
                DispatchQueue.main.async {
                    if let data = data {
                        
                        cell?.moviePosterImageView?.image = UIImage(data: data)
                        
                        cell?.moviePosterImageView?.contentMode = UIView.ContentMode.scaleAspectFit
                    }
                }
            }).resume()
            
            
            
            
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        if (self.moviesWrapper != nil)
        {
            guard let page =  self.moviesWrapper!.page, let totalPages =  self.moviesWrapper!.totalPages else
            {
                print("Some paremter is nil")
                return
            }
            if (page < totalPages)
            {
                self.loadMovies()
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // alternate row colors
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0) // very light gray
        } else {
            cell.backgroundColor = UIColor.white
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let moviesDetailVC = segue.destination as? MoviesDetailViewController {
            if let indexPath = self.listTableView?.indexPathForSelectedRow {
                let movieDetail = self.filteredMovies![indexPath.row]
                moviesDetailVC.moviesDetail = movieDetail
                moviesDetailVC.title = movieDetail.title
                
            }
        }
    }
    
    @IBAction func filerSelected(_ sender: Any) {
        
        
        if (self.selectedTextField != nil) && self.selectedTextField!.isFirstResponder
        {
            let fromYear = Int(self.fromYearTextField.text!) ?? 0
            let toYear = Int(self.toYearTextField.text!) ?? 0
            self.filteredMovies = self.movies?.filter { movie in
                return movie.ReleaseYear >= fromYear && movie.ReleaseYear <= toYear
            }
            if validateFilterTextFieldsWith() {
                self.listTableView?.reloadData()
                
                self.selectedTextField? .resignFirstResponder()
                
            }
            else
            {
                let alert = UIAlertController (title: "Invalid Input", message: "Enter valid year", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
                    //Cancel Action
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
            
            
            return
            
        }
        
        validateFilterTextFieldsWith()
        self.fromYearTextField.becomeFirstResponder()
        
        
        
        
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.filterToolBar.isHidden = false
        return true
    } // return NO to disallow editing.
    
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
        self.selectedTextField = textField
    } // became first responder
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        self.filterToolBar.isHidden = true
        return true
    } // return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
    
    
    
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason){} // if implemented, called in place of textFieldDidEndEditing:
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    } // return NO to not change text
    
    
    
    
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool{
        return true
    } // called when clear button pressed. return NO to ignore (no notifications)
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{ // called when 'return' key pressed.
        return true
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    override var inputAccessoryView: UIView?
    {
        return self.filterToolBar;
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func getMaximumAndMinimum ()  -> (oldMovieRelease:MoviesList, latestMovieRelease:MoviesList) {
        
        let latestMovieRelease =  self.movies!.max(by: { (a, b) -> Bool in
            return a.ReleaseYear < b.ReleaseYear
        })
        let oldMovieRelease =  self.movies!.min(by: { (a, b) -> Bool in
            return a.ReleaseYear < b.ReleaseYear
        })
        
        
        return (oldMovieRelease!, latestMovieRelease!)
        
    }
    
    func validateFilterTextFieldsWith() -> Bool
    {
        let movieWithMaxAndMinYears = getMaximumAndMinimum()
        
        
        if (self.fromYearTextField.text == "" || Int(self.fromYearTextField.text!)! <= 0 || Int(self.fromYearTextField.text!)! >  movieWithMaxAndMinYears.latestMovieRelease.ReleaseYear ||  Int(self.fromYearTextField.text!)! < movieWithMaxAndMinYears.oldMovieRelease.ReleaseYear)
        {
            self.fromYearTextField.text = String(describing: movieWithMaxAndMinYears.oldMovieRelease.ReleaseYear ?? 0)
            return false
        }
        if ( self.toYearTextField.text == "" || Int(self.toYearTextField.text!)! <= 0 || Int(self.toYearTextField.text!)! >  movieWithMaxAndMinYears.latestMovieRelease.ReleaseYear ||  Int(self.toYearTextField.text!)! < movieWithMaxAndMinYears.oldMovieRelease.ReleaseYear)
        {
            self.toYearTextField.text = String(describing: movieWithMaxAndMinYears.latestMovieRelease.ReleaseYear ?? 0)
            return false
            
        }
        return true
    }
}
